% Code

# Threads

## Threads erstellen

```java
import java.io.*;

new Thread() {
	public void run() {
		while (true) {
			System.out.println("Penis");
		}
	}
}.start(); // WICHTIG!
```

## Threads terminieren

```java
import java.io.*;

Thread t = new Thread() {
	public void run() {
		while (!this.isInterrupted()) {
			System.out.println("Penis");
		}
	}
}
t.start(); // WICHTIG!
t.interrupt();
```

# Netzwerk

## Server

```java
import lejos.remote.nxt.*;
import java.io.*;

SocketConnector sock = new SocketConnector();
NXTConnection ev3 = sock.waitForConnection(60000, 0);
DataOutputStream stream = ev3.openDataOutputStream();

stream.writeInt(69);
stream.flush(); // WICHTIG!
```

## Client

```java
import lejos.remote.nxt.*;
import java.io.*;

SocketConnector sock = new SocketConnector();
NXTConnection ev3 = sock.connect("10.0.1.1", 0);
DataInputStream stream = ev.openDataInputStream();

int data = stream.readInt();
System.out.println(data); // 69
```

# MySQL

```java
import java.sql.*;
javax.management.Query;

String url = "jdbc:mariadb://HOSTNAME/DBNAME";
Connection connection = DriverManager.getConnection(url, "username", "password");

// SELECT
Statement = connection.createStatement();
ResultSet result = statement.executeQuery("SELECT * FROM foo;");
while (result.next()) {
	int id = result.getInt(1);
	String name = result.getString(2);
	// ...
}

// INSERT/DELETE/DROP/...
statement.execute("DELETE FROM foo;");
```
